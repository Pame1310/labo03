#!/bin/bash
nouser=`who | wc -l`
echo -e "User name: $USER (Login name: $LOGNAME)" 
echo -e "Current Shell: $SHELL"  
echo -e "Home Directory: $HOME" 
echo -e "Your O/s Type: $OSTYPE" 
echo -e "PATH: $PATH" 
echo -e "Current directory: `pwd`" 
echo -e "Currently Logged: $nouser user(s)" 
echo -e "`cat /etc/users`"
echo -e "OS: `cat /etc/redhat-release`"
echo -e "Release number: `cat /etc/*-release`"
echo -e "Kernel: cat /proc/version"
echo -e "`cat /etc/shells`"
echo -e "`cat /etc/sysconfig/mouse`"
echo -e "`cat /proc/cpuinfo`"
echo -e  "`cat /proc/meminfo`"
echo -e "Hard disk information:"  
echo -e "Model: `cat /proc/ide/hda/model` "    
echo -e "Driver: `cat /proc/ide/hda/driver` "   
echo -e "Cache size: `cat /proc/ide/hda/cache` "
echo -e "File mounted: `cat /proc/mounts` " 
 
 


