#!/bin/bash
rev=0
sd=0
a=$1 
or=$1
while [ $a -gt 0 ]
do
        sd=`expr $a % 10`
        temp=`expr $rev \* 10`
        rev=`expr $temp + $sd`
        a=`expr $a / 10`
done
 
echo "Reverso de $or es $rev"
