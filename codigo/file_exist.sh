#!/bin/bash
if [ $# -eq 0 ]
then
	echo "ERROR: Por favor escriba el nombre del archivo"
else
	#Checking if the file exists or not
	if [ -f $1 ]
	then
		echo "El archivo existe";
	else
		echo "El archivo no  existe";
	fi
fi
exit 0;
